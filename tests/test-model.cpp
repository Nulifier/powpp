#include "powpp/generator.h"
#include "powpp/line.h"
#include "powpp/network.h"
#include "powpp/shunt.h"
#include "powpp/types.h"
#include "test-models.h"

powpp::Network models::lynnPowellWithoutGenerator() {
	using powpp::BusId;

	powpp::Network net;

	// Base voltage is 132kV
	const powpp::Scalar Vbase = 132000.0;
	// Base power in the book is 100MVA
	const powpp::Scalar Sbase = 100;
	const powpp::Scalar Zbase = Vbase * Vbase / Sbase;

	net.buses.emplace(1, powpp::Bus(Vbase));
	net.buses.emplace(2, powpp::Bus(Vbase));
	net.buses.emplace(3, powpp::Bus(Vbase));
	net.buses.emplace(4, powpp::Bus(Vbase));
	net.buses.emplace(5, powpp::Bus(Vbase));

	net.loads.emplace(2, powpp::Load(BusId(2), powpp::Complex(40, 20)));
	net.loads.emplace(3, powpp::Load(BusId(3), powpp::Complex(25, 15)));
	net.loads.emplace(4, powpp::Load(BusId(4), powpp::Complex(40, 20)));
	net.loads.emplace(5, powpp::Load(BusId(5), powpp::Complex(50, 20)));

	powpp::LineType lineTypeA(0.05 * Zbase, 0.11 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeB(0.03 * Zbase, 0.08 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeC(0.04 * Zbase, 0.09 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeD(0.06 * Zbase, 0.13 * Zbase, 0.03 / Zbase);

	net.lines.emplace(1, powpp::Line(BusId(1), BusId(2), lineTypeA, 1.0));
	net.lines.emplace(2, powpp::Line(BusId(1), BusId(3), lineTypeA, 1.0));
	net.lines.emplace(3, powpp::Line(BusId(1), BusId(5), lineTypeB, 1.0));
	net.lines.emplace(4, powpp::Line(BusId(2), BusId(3), lineTypeC, 1.0));
	net.lines.emplace(5, powpp::Line(BusId(2), BusId(5), lineTypeC, 1.0));
	net.lines.emplace(6, powpp::Line(BusId(3), BusId(4), lineTypeD, 1.0));
	net.lines.emplace(7, powpp::Line(BusId(4), BusId(5), lineTypeC, 1.0));

	return net;
}

powpp::Network models::lynnPowellWithGenerator() {
	using powpp::BusId;

	powpp::Network net;

	// Base voltage is 132kV
	const powpp::Scalar Vbase = 132000.0;
	// Base power in the book is 100MVA
	const powpp::Scalar Sbase = 100;
	const powpp::Scalar Zbase = Vbase * Vbase / Sbase;

	net.buses.emplace(1, powpp::Bus(Vbase));
	net.buses.emplace(2, powpp::Bus(Vbase));
	net.buses.emplace(3, powpp::Bus(Vbase));
	net.buses.emplace(4, powpp::Bus(Vbase));
	net.buses.emplace(5, powpp::Bus(Vbase));

	net.loads.emplace(2, powpp::Load(BusId(2), powpp::Complex(40, 20)));
	net.loads.emplace(3, powpp::Load(BusId(3), powpp::Complex(25, 15)));
	//net.loads.emplace(4, powpp::Load(BusId(4), powpp::Complex(30,  0)));
	net.loads.emplace(5, powpp::Load(BusId(5), powpp::Complex(50, 20)));

	net.generators.emplace(4, powpp::Generator(BusId(4), 30.0, 1.0, -15.0, 20.0));

	powpp::LineType lineTypeA(0.05 * Zbase, 0.11 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeB(0.03 * Zbase, 0.08 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeC(0.04 * Zbase, 0.09 * Zbase, 0.02 / Zbase);
	powpp::LineType lineTypeD(0.06 * Zbase, 0.13 * Zbase, 0.03 / Zbase);

	net.lines.emplace(1, powpp::Line(BusId(1), BusId(2), lineTypeA, 1.0));
	net.lines.emplace(2, powpp::Line(BusId(1), BusId(3), lineTypeA, 1.0));
	net.lines.emplace(3, powpp::Line(BusId(1), BusId(5), lineTypeB, 1.0));
	net.lines.emplace(4, powpp::Line(BusId(2), BusId(3), lineTypeC, 1.0));
	net.lines.emplace(5, powpp::Line(BusId(2), BusId(5), lineTypeC, 1.0));
	net.lines.emplace(6, powpp::Line(BusId(3), BusId(4), lineTypeD, 1.0));
	net.lines.emplace(7, powpp::Line(BusId(4), BusId(5), lineTypeC, 1.0));

	return net;
}

powpp::Network models::ieee14() {
	using powpp::BusId;
	using namespace std::complex_literals;

	powpp::Network net;

	// Base voltage is 13.8kV
	const powpp::Scalar Vbase = 13800.0;
	// Base power in the book is 100MVA
	const powpp::Scalar Sbase = 100;
	const powpp::Scalar Zbase = Vbase * Vbase / Sbase;

	net.buses.emplace( 1, powpp::Bus(Vbase));
	net.buses.emplace( 2, powpp::Bus(Vbase));
	net.buses.emplace( 3, powpp::Bus(Vbase));
	net.buses.emplace( 4, powpp::Bus(Vbase));
	net.buses.emplace( 5, powpp::Bus(Vbase));
	net.buses.emplace( 6, powpp::Bus(Vbase));
	net.buses.emplace( 7, powpp::Bus(Vbase));
	net.buses.emplace( 8, powpp::Bus(Vbase));
	net.buses.emplace( 9, powpp::Bus(Vbase));
	net.buses.emplace(10, powpp::Bus(Vbase));
	net.buses.emplace(11, powpp::Bus(Vbase));
	net.buses.emplace(12, powpp::Bus(Vbase));
	net.buses.emplace(13, powpp::Bus(Vbase));
	net.buses.emplace(14, powpp::Bus(Vbase));

	net.loads.emplace( 2, powpp::Load(BusId( 2), 21.7 + 12.7i));
	net.loads.emplace( 3, powpp::Load(BusId( 3), 94.2 + 19.0i));
	net.loads.emplace( 4, powpp::Load(BusId( 4), 47.8 -  3.9i));
	net.loads.emplace( 5, powpp::Load(BusId( 5),  7.6 +  1.6i));
	net.loads.emplace( 6, powpp::Load(BusId( 6), 11.2 +  7.5i));
	net.loads.emplace( 9, powpp::Load(BusId( 9), 29.5 + 16.6i));
	net.loads.emplace(10, powpp::Load(BusId(10),  9.0 +  5.8i));
	net.loads.emplace(11, powpp::Load(BusId(11),  3.5 +  1.8i));
	net.loads.emplace(12, powpp::Load(BusId(12),  6.1 +  1.6i));
	net.loads.emplace(13, powpp::Load(BusId(13), 13.5 +  5.8i));
	net.loads.emplace(14, powpp::Load(BusId(14), 14.9 +  5.0i));

	net.generators.emplace(1, powpp::Generator(BusId(1), 232.4, 1.06, 0.0, 10.0));
	net.generators.emplace(2, powpp::Generator(BusId(2), 40.0, 1.045, -40.0, 50.0));
	net.generators.emplace(3, powpp::Generator(BusId(3), 0.0, 1.01, 0.0, 40.0));
	net.generators.emplace(6, powpp::Generator(BusId(6), 0.0, 1.07, -6.0, 24.0));
	net.generators.emplace(8, powpp::Generator(BusId(8), 0.0, 1.09, -6.0, 24.0));

	net.shunts.emplace(9, powpp::Shunt(BusId(9), 0.0 + 19.0i));

	net.lines.emplace( 1, powpp::Line(BusId( 1), BusId( 2), 0.01938, 0.05917, 0.0528));
	net.lines.emplace( 2, powpp::Line(BusId( 1), BusId( 5), 0.05403, 0.22304, 0.0492));
	net.lines.emplace( 3, powpp::Line(BusId( 2), BusId( 3), 0.04699, 0.19797, 0.0438));
	net.lines.emplace( 4, powpp::Line(BusId( 2), BusId( 4), 0.05811, 0.17632, 0.034));
	net.lines.emplace( 5, powpp::Line(BusId( 2), BusId( 5), 0.05695, 0.17388, 0.0346));
	net.lines.emplace( 6, powpp::Line(BusId( 3), BusId( 4), 0.06701, 0.17103, 0.0128));
	net.lines.emplace( 7, powpp::Line(BusId( 4), BusId( 5), 0.01335, 0.04211, 0.0));
	net.lines.emplace( 8, powpp::Line(BusId( 4), BusId( 7), 0.0,     0.20912, 0.0));
	net.lines.emplace( 9, powpp::Line(BusId( 4), BusId( 9), 0.0,     0.55618, 0.0));
	net.lines.emplace(10, powpp::Line(BusId( 5), BusId( 6), 0.0,     0.25202, 0.0));
	net.lines.emplace(11, powpp::Line(BusId( 6), BusId(11), 0.09498, 0.19890, 0.0));
	net.lines.emplace(12, powpp::Line(BusId( 6), BusId(12), 0.12291, 0.25581, 0.0));
	net.lines.emplace(13, powpp::Line(BusId( 6), BusId(13), 0.06615, 0.13027, 0.0));
	net.lines.emplace(14, powpp::Line(BusId( 7), BusId( 8), 0.0,     0.17615, 0.0));
	net.lines.emplace(15, powpp::Line(BusId( 7), BusId( 9), 0.0,     0.11001, 0.0));
	net.lines.emplace(16, powpp::Line(BusId( 9), BusId(10), 0.03181, 0.08450, 0.0));
	net.lines.emplace(17, powpp::Line(BusId( 9), BusId(14), 0.12711, 0.27038, 0.0));
	net.lines.emplace(18, powpp::Line(BusId(10), BusId(11), 0.08205, 0.19207, 0.0));
	net.lines.emplace(19, powpp::Line(BusId(12), BusId(13), 0.22092, 0.19988, 0.0));
	net.lines.emplace(20, powpp::Line(BusId(13), BusId(14), 0.17093, 0.34802, 0.0));

	return net;
}

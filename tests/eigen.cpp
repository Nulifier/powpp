#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <powpp/eigen.h>
#include <powpp/table.h>
#include <powpp/types.h>
#include <vector>

template <typename Derived>
void printMat(const Eigen::EigenBase<Derived>& mat) {
	powpp::Table t;
	t.setData(mat);
	t.print();
}

TEST_CASE("eigen::removeCross(Dense)", "[eigen][util]") {
	powpp::Mat mat(5, 5);
	// clang-format off
	mat <<  0,  1,  2,  3,  4,
	        5,  6,  7,  8,  9,
		   10, 11, 12, 13, 14,
		   15, 16, 17, 18, 19,
		   20, 21, 22, 23, 24;
	// clang-format on

	// Make sure the data went in correctly
	REQUIRE(mat.sum() == 300.0);
	REQUIRE(mat.row(0).sum() == 10.0);

	SECTION("Middle") {
		// Remove the second row and third column
		powpp::eigen::removeCross(mat, 1, 3);

		REQUIRE(mat.rows() == 4);
		REQUIRE(mat.cols() == 4);
		REQUIRE(mat.sum() == 208.0);
		REQUIRE(mat.coeff(2, 3) == 19.0);
	}

	SECTION("Begining") {
		// Remove the first row and column
		powpp::eigen::removeCross(mat, 0, 0);

		REQUIRE(mat.rows() == 4);
		REQUIRE(mat.cols() == 4);
		REQUIRE(mat.sum() == 240.0);
		REQUIRE(mat.coeff(1, 0) == 11.0);
	}

	SECTION("End") {
		// Remove the last row and column
		powpp::eigen::removeCross(mat, 4, 4);

		REQUIRE(mat.rows() == 4);
		REQUIRE(mat.cols() == 4);
		REQUIRE(mat.sum() == 144.0);
		REQUIRE(mat.coeff(3, 3) == 18.0);
	}
}

TEST_CASE("eigen::expandCross(Dense)", "[eigen][util]") {
	powpp::Mat mat(5, 5);
	// clang-format off
	mat <<  0,  1,  2,  3,  4,
	        5,  6,  7,  8,  9,
		   10, 11, 12, 13, 14,
		   15, 16, 17, 18, 19,
		   20, 21, 22, 23, 24;
	// clang-format on

	// Make sure the data went in correctly
	REQUIRE(mat.sum() == 300.0);
	REQUIRE(mat.row(0).sum() == 10.0);

	SECTION("Middle") {
		// Expand the second row and fourth column
		powpp::eigen::expandCross(mat, 1, 3);

		REQUIRE(mat.rows() == 6);
		REQUIRE(mat.cols() == 6);
		REQUIRE(mat.sum() == 300.0);
		REQUIRE(mat.coeff(1, 3) == 0.0);
	}

	SECTION("Begining") {
		// Expand the first row and column
		powpp::eigen::expandCross(mat, 0, 0);

		REQUIRE(mat.rows() == 6);
		REQUIRE(mat.cols() == 6);
		REQUIRE(mat.sum() == 300.0);
		REQUIRE(mat.coeff(0, 0) == 0.0);
	}

	SECTION("End") {
		// Expand the last row and column
		powpp::eigen::expandCross(mat, 5, 5);

		REQUIRE(mat.rows() == 6);
		REQUIRE(mat.cols() == 6);
		REQUIRE(mat.sum() == 300.0);
		REQUIRE(mat.coeff(4, 4) == 24.0);
	}
}

TEST_CASE("eigen::removeCross(Sparse)", "[eigen][util]") {
	using Triplet = Eigen::Triplet<powpp::Scalar>;
	// clang-format off
	std::vector<Triplet> data = {
		Triplet(0, 2, 3),
		Triplet(1, 4, 1),
		Triplet(2, 0, 1),
		Triplet(2, 3, 2),
		Triplet(4, 4, 1)
	};
	// clang-format on

	powpp::SpMat mat(5, 5);
	mat.setFromTriplets(data.begin(), data.end());

	// Make sure the data went in correctly
	REQUIRE(mat.nonZeros() == 5);
	REQUIRE(mat.sum() == 8.0);

	SECTION("Middle") {
		// Remove the second row and third column
		powpp::SpMat out;
		powpp::eigen::removeCross(out, mat, 1, 3);

		REQUIRE(out.rows() == 4);
		REQUIRE(out.cols() == 4);
		REQUIRE(out.sum() == 5.0);
		REQUIRE(out.coeff(2, 3) == 0.0);
	}

	SECTION("Begining") {
		// Remove the first row and column
		powpp::SpMat out;
		powpp::eigen::removeCross(out, mat, 0, 0);

		REQUIRE(out.rows() == 4);
		REQUIRE(out.cols() == 4);
		REQUIRE(out.sum() == 4.0);
		REQUIRE(out.coeff(2, 0) == 0.0);
	}

	SECTION("End") {
		// Remove the last row and column
		powpp::SpMat out;
		powpp::eigen::removeCross(out, mat, 4, 4);

		REQUIRE(out.rows() == 4);
		REQUIRE(out.cols() == 4);
		REQUIRE(out.sum() == 6.0);
		REQUIRE(out.coeff(2, 3) == 2.0);
	}
}

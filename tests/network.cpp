#include "powpp/table.h"
#include "powpp/types.h"
#include "test-models.h"
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <iterator>
#include <powpp/network.h>

using Catch::Approx;
using powpp::BusId;
using powpp::Complex;
using powpp::LoadId;
using powpp::Network;

TEST_CASE("Network Models", "[core]") {
	Network net = models::lynnPowellWithoutGenerator();

	REQUIRE(net.buses.size() == 5);
	for (auto [id, bus] : net.buses) {
		REQUIRE(bus.nominalVoltage == Approx(132000.0));
	}

	REQUIRE(net.loads.size() == 4);
	REQUIRE(net.loads.at(LoadId(2)).power == Complex(40.0, 20.0));
	REQUIRE(net.loads.at(LoadId(3)).power == Complex(25.0, 15.0));
	REQUIRE(net.loads.at(LoadId(4)).power == Complex(40.0, 20.0));
	REQUIRE(net.loads.at(LoadId(5)).power == Complex(50.0, 20.0));

	REQUIRE(net.lines.size() == 7);
}

TEST_CASE("Network Y matrix", "[core]") {
	Network net = models::lynnPowellWithoutGenerator();

	net.compile(BusId(1), 100.0);

	powpp::Table table;
	table.setData(net.getY());

	using powpp::Index;
	Index bus1 = net.getBusIndex(BusId(1));
	Index bus2 = net.getBusIndex(BusId(2));
	Index bus3 = net.getBusIndex(BusId(3));
	Index bus4 = net.getBusIndex(BusId(4));
	Index bus5 = net.getBusIndex(BusId(5));

	const powpp::SpMatCx& Y = net.getY();

	// Diagonals
	REQUIRE(Y.coeff(bus1, bus1).real() == Approx(10.958904));
	REQUIRE(Y.coeff(bus1, bus1).imag() == Approx(-25.997397));
	REQUIRE(Y.coeff(bus2, bus2).real() == Approx(11.672080));
	REQUIRE(Y.coeff(bus2, bus2).imag() == Approx(-26.060948));

	// Off-diagonals
	REQUIRE(Y.coeff(bus1, bus2).real() == Approx(-3.424658));
	REQUIRE(Y.coeff(bus1, bus2).imag() == Approx(7.534247));
	REQUIRE(Y.coeff(bus1, bus3).real() == Approx(-3.424658));
	REQUIRE(Y.coeff(bus1, bus3).imag() == Approx(7.534247));
	REQUIRE(Y.coeff(bus1, bus4).real() == Approx(0.0));
	REQUIRE(Y.coeff(bus1, bus4).imag() == Approx(0.0));
	REQUIRE(Y.coeff(bus1, bus5).real() == Approx(-4.109589));
	REQUIRE(Y.coeff(bus1, bus5).imag() == Approx(10.958904));
}

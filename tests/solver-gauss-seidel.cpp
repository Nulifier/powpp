#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <powpp/solver-jacobi.h>
#include "powpp/solver-gauss-seidel.h"
#include "powpp/solver-gauss-seidel-z.h"
#include "powpp/solver.h"
#include "test-models.h"
#include <iostream>

TEST_CASE("Gauss Seidel Solver", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverGaussSeidel solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Gauss Seidel Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.954465));
	REQUIRE(sol.getV(1).imag() == Approx(-0.040085));
	REQUIRE(sol.getV(2).real() == Approx( 0.954005));
	REQUIRE(sol.getV(2).imag() == Approx(-0.039381));
}

TEST_CASE("Gauss Seidel Solver with Gens", "[core]") {
	powpp::Network net = models::lynnPowellWithGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverGaussSeidel solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Gauss Seidel Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.969656));
	REQUIRE(sol.getV(1).imag() == Approx(-0.0246445));
	REQUIRE(sol.getV(2).real() == Approx( 0.974445));
	REQUIRE(sol.getV(2).imag() == Approx(-0.018254));
}

TEST_CASE("Gauss Seidel Solver Z-Bus", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverGaussSeidelZ solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Gauss Seidel Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.954465));
	REQUIRE(sol.getV(1).imag() == Approx(-0.040085));
	REQUIRE(sol.getV(2).real() == Approx( 0.954005));
	REQUIRE(sol.getV(2).imag() == Approx(-0.039381));
}

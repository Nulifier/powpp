#include <catch2/catch_test_macros.hpp>
#include <functional>
#include <powpp/strong-typedef.h>

STRONG_TYPEDEF(TypeA, int);
STRONG_TYPEDEF(TypeB, int);

STRONG_TYPEDEF_HASH(TypeA);
STRONG_TYPEDEF_HASH(TypeB);

TEST_CASE("Strong Typedef", "[util]") {
	TypeA a1(1);
	TypeA a1_(1);
	TypeA a2(2);

	REQUIRE(a1 == a1_);
	REQUIRE(a1 != a2);

	a1_ = a1;

	REQUIRE(std::hash<TypeA>{}(a1) == std::hash<TypeA>{}(a1_));
}

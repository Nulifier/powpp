#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <powpp/solver-fast-decoupled.h>
#include "test-models.h"
#include <iostream>

TEST_CASE("Fast Decoupled Solver", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverFastDecoupled solver(net);

	const auto& sol = solver.getSolution();

	auto result = solver.solve();

	/*
	std::cout << "Fast Decoupled Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getVmag(1) == Approx( 0.955310));
	REQUIRE(sol.getVarg(1) == Approx(-0.041972));
	REQUIRE(sol.getVmag(2) == Approx( 0.954823));
	REQUIRE(sol.getVarg(2) == Approx(-0.041256));
	REQUIRE(sol.getVmag(3) == Approx( 0.933344));
	REQUIRE(sol.getVarg(3) == Approx(-0.063701));
	REQUIRE(sol.getVmag(4) == Approx( 0.953399));
	REQUIRE(sol.getVarg(4) == Approx(-0.046931));
}

TEST_CASE("Fast Decoupled Solver with Gens", "[core]") {
	powpp::Network net = models::lynnPowellWithGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverFastDecoupled solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Fast Decoupled Solver with Gens" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getVreal(1) == Approx( 0.974707));
	REQUIRE(sol.getVimag(1) == Approx(-0.026672));
	REQUIRE(sol.getVreal(2) == Approx( 0.981296));
	REQUIRE(sol.getVimag(2) == Approx(-0.021197));
}

TEST_CASE("Fast Decoupled Solver with Gens (IEEE 14)", "[core][.]") {
	powpp::Network net = models::ieee14();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverFastDecoupled solver(net);
	solver.tolerance = 1e-8;
	solver.maxIterations = 400;

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Fast Decoupled Solver with Gens" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	//REQUIRE(sol.getVreal(1) == Approx( 0.974707));
	//REQUIRE(sol.getVimag(1) == Approx(-0.026672));
	//REQUIRE(sol.getVreal(2) == Approx( 0.981296));
	//REQUIRE(sol.getVimag(2) == Approx(-0.021197));
}

#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <powpp/solver-jacobi.h>
#include <powpp/solver-jacobi-z.h>
#include "powpp/network.h"
#include "test-models.h"

TEST_CASE("Jacobi Solver", "[core]") {
	powpp::Network net;

	net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverJacobi solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Jacobi Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.95446472));
	REQUIRE(sol.getV(1).imag() == Approx(-0.04008461));
}

TEST_CASE("Jacobi Solver with Gens", "[core]") {
	powpp::Network net;

	net = models::lynnPowellWithGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverJacobi solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Jacobi Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.974755));
	REQUIRE(sol.getV(1).imag() == Approx(-0.024508));
	REQUIRE(sol.getV(2).real() == Approx( 0.981345));
	REQUIRE(sol.getV(2).imag() == Approx(-0.018187));
}

TEST_CASE("Jacobi Solver Z-Bus", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverJacobiZ solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Jacobi Solver Z-Bus" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.95446472));
	REQUIRE(sol.getV(1).imag() == Approx(-0.04008461));
	REQUIRE(sol.getV(2).real() == Approx( 0.954007));
	REQUIRE(sol.getV(2).imag() == Approx(-0.03938094));
}

TEST_CASE("Jacobi Solver Z-Bus with Gens", "[core]") {
	powpp::Network net = models::lynnPowellWithGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverJacobiZ solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Jacobi Solver Z-Bus" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getV(1).real() == Approx( 0.969594));
	REQUIRE(sol.getV(1).imag() == Approx(-0.0247766));
	REQUIRE(sol.getV(2).real() == Approx( 0.974362));
	REQUIRE(sol.getV(2).imag() == Approx(-0.0184364));
}

#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <powpp/solver-newton-rect.h>
#include <powpp/solver-newton-polar.h>
#include "test-models.h"
#include <iostream>

TEST_CASE("Newton Raphson Rect Solver", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverNewtonRect solver(net);

	auto result = solver.solve();

	/*
	std::cout << "Newton Raphson Rect Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);
}

TEST_CASE("Newton Raphson Polar Solver", "[core]") {
	powpp::Network net = models::lynnPowellWithoutGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverNewtonPolar solver(net);

	const auto& sol = solver.getSolution();

	auto result = solver.solve();

	/*
	std::cout << "Newton Raphson Polar Solver" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getVmag(1) == Approx( 0.955310));
	REQUIRE(sol.getVarg(1) == Approx(-0.041972));
	REQUIRE(sol.getVmag(2) == Approx( 0.954823));
	REQUIRE(sol.getVarg(2) == Approx(-0.041256));
	REQUIRE(sol.getVmag(3) == Approx( 0.933344));
	REQUIRE(sol.getVarg(3) == Approx(-0.063701));
	REQUIRE(sol.getVmag(4) == Approx( 0.953399));
	REQUIRE(sol.getVarg(4) == Approx(-0.046931));
}

TEST_CASE("Newton Raphson Polar Solver with Gens", "[core]") {
	powpp::Network net = models::lynnPowellWithGenerator();

	net.compile(powpp::BusId(1), 100.0);

	powpp::SolverNewtonPolar solver(net);

	auto result = solver.solve();

	const auto& sol = solver.getSolution();

	/*
	std::cout << "Newton Raphson Polar Solver with Gens" << std::endl;
	std::cout << "Iterations " << solver.getIterationCount() << std::endl;
	std::cout << "Time       " << solver.getSolveDuration().count() << "μs" << std::endl;
	//*/

	REQUIRE(result == powpp::Solver::Result::Solved);

	using Catch::Approx;

	REQUIRE(sol.getVreal(1) == Approx( 0.974707));
	REQUIRE(sol.getVimag(1) == Approx(-0.026672));
	REQUIRE(sol.getVreal(2) == Approx( 0.981296));
	REQUIRE(sol.getVimag(2) == Approx(-0.021197));
}

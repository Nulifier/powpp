#pragma once

#include <powpp/network.h>

namespace models {
	powpp::Network lynnPowellWithoutGenerator();
	powpp::Network lynnPowellWithGenerator();
	powpp::Network ieee14();
}

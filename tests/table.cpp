#include "powpp/types.h"
#include <catch2/catch_test_macros.hpp>
#include <powpp/table.h>

TEST_CASE("Table", "[util]") {
	powpp::Table table;

	SECTION("Vec") {
		powpp::Vec v(3);
		v << 1, 2, 3;

		table.setData(v);

		const char* str =
			"1.000000\n"
			"2.000000\n"
			"3.000000";

		REQUIRE(table.str() == str);
	}

	SECTION("VecCx") {
		using namespace std::complex_literals;

		powpp::VecCx v(3);
		v << 1.0 + 3.0i,
		     2.0 + 2.0i,
			 3.0 + 1.0i;

		table.setData(v);

		const char* str =
			"1.000000+3.000000j\n"
			"2.000000+2.000000j\n"
			"3.000000+1.000000j";

		REQUIRE(table.str() == str);
	}

	SECTION("Mat") {
		powpp::Mat m(3, 2);
		m << 1, 2, 3,
		     4, 5, 6;

		table.setData(m);

		const char* str =
			"1.000000 2.000000\n"
			"3.000000 4.000000\n"
			"5.000000 6.000000";

		REQUIRE(table.str() == str);
	}
}

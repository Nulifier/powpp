#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <limits>
#include <powpp/types.h>
#include <powpp/math.h>

using Catch::Approx;
using powpp::Complex;
using powpp::Scalar;

TEST_CASE("Angular Frequency", "[math]") {
	REQUIRE(powpp::math::angularFrequency(60) == Approx(376.9911184308));
}

TEST_CASE("Resistor Impedance", "[math]") {
	Complex i = powpp::math::resistorImpedance(42.0);
	REQUIRE(i.real() == Approx(42.0));
	REQUIRE(i.imag() == Approx(0.0));
}

TEST_CASE("Capacitor Impedance", "[math]") {
	Complex i = powpp::math::capacitorImpedance(100e-6, 60.0);
	REQUIRE(i.real() == Approx(0.0));
	REQUIRE(i.imag() == Approx(-26.5258238486));
}

TEST_CASE("Inductor Impedance", "[math]") {
	Complex i = powpp::math::inductorImpedance(10e-3, 60.0);
	REQUIRE(i.real() == Approx(0.0));
	REQUIRE(i.imag() == Approx(3.76991));
}

TEST_CASE("Admittance to impedance", "[math]") {
	Complex z = powpp::math::admittanceToImpedance(Complex(250.0, 754.0));
	REQUIRE(z.real() == Approx(0.0003961865));
	REQUIRE(z.imag() == Approx(-0.0011948984));
}

TEST_CASE("Impedance to admittance", "[math]") {
	Complex z = powpp::math::impedanceToAdmittance(Complex(2.0, 5.0));
	REQUIRE(z.real() == Approx(0.0689655172));
	REQUIRE(z.imag() == Approx(-0.1724137931));
}

TEST_CASE("Nearly Equal", "[math]") {
	using powpp::math::nearlyEqual;

	// Regular large numbers
	REQUIRE(nearlyEqual(1000000.0, 1000001.0));
	REQUIRE(nearlyEqual(1000001.0, 1000000.0));
	REQUIRE_FALSE(nearlyEqual(10000.0, 10001.0));
	REQUIRE_FALSE(nearlyEqual(10001.0, 10000.0));

	// Negative large numbers
	REQUIRE(nearlyEqual(-1000000.0, -1000001.0));
	REQUIRE(nearlyEqual(-1000001.0, -1000000.0));
	REQUIRE_FALSE(nearlyEqual(-10000.0, -10001.0));
	REQUIRE_FALSE(nearlyEqual(-10001.0, -10000.0));

	// Numbers around 1
	REQUIRE(nearlyEqual(1.0000001, 1.0000002));
	REQUIRE(nearlyEqual(1.0000002, 1.0000001));
	REQUIRE_FALSE(nearlyEqual(1.0002, 1.0001));
	REQUIRE_FALSE(nearlyEqual(1.0001, 1.0002));

	// Numbers around -1
	REQUIRE(nearlyEqual(-1.0000001, -1.0000002));
	REQUIRE(nearlyEqual(-1.0000002, -1.0000001));
	REQUIRE_FALSE(nearlyEqual(-1.0002, -1.0001));
	REQUIRE_FALSE(nearlyEqual(-1.0001, -1.0002));

	// Numbers between 1 and 0
	REQUIRE(nearlyEqual(0.000000001000001, 0.000000001000002));
	REQUIRE(nearlyEqual(0.000000001000002, 0.000000001000001));
	REQUIRE_FALSE(nearlyEqual(0.000000000001002, 0.000000000001001));
	REQUIRE_FALSE(nearlyEqual(0.000000000001001, 0.000000000001002));

	// Numbers between -1 and 0
	REQUIRE(nearlyEqual(-0.000000001000001, -0.000000001000002));
	REQUIRE(nearlyEqual(-0.000000001000002, -0.000000001000001));
	REQUIRE_FALSE(nearlyEqual(-0.000000000001002, -0.000000000001001));
	REQUIRE_FALSE(nearlyEqual(-0.000000000001001, -0.000000000001002));

	// Small differences away from zero
	REQUIRE(nearlyEqual(0.3, 0.30000003));
	REQUIRE(nearlyEqual(-0.3, -0.30000003));

	// Comparisons involving zero
	REQUIRE(nearlyEqual(0.0, 0.0));
	REQUIRE(nearlyEqual(0.0, -0.0));
	REQUIRE(nearlyEqual(-0.0, -0.0));
	REQUIRE_FALSE(nearlyEqual(0.00000001, 0.0));
	REQUIRE_FALSE(nearlyEqual(0.0, 0.00000001));
	REQUIRE_FALSE(nearlyEqual(-0.00000001, 0.0));
	REQUIRE_FALSE(nearlyEqual(0.0, -0.00000001));

	REQUIRE(nearlyEqual(0.0, 1e-310, 0.01));
	REQUIRE(nearlyEqual(1e-310, 0.0, 0.01));
	REQUIRE_FALSE(nearlyEqual(1e-310, 0.0, 0.000001));
	REQUIRE_FALSE(nearlyEqual(0.0, 1e-310, 0.000001));

	REQUIRE(nearlyEqual(0.0, -1e-310, 0.1));
	REQUIRE(nearlyEqual(-1e-310, 0.0, 0.1));
	REQUIRE_FALSE(nearlyEqual(-1e-40, 0.0, 0.00000001));
	REQUIRE_FALSE(nearlyEqual(0.0, -1e-40, 0.00000001));

	// Comparisons involving extreme values
	constexpr powpp::Scalar MAX_VALUE = std::numeric_limits<powpp::Scalar>::max();
	REQUIRE(nearlyEqual(MAX_VALUE, MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(MAX_VALUE, -MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(-MAX_VALUE, MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(MAX_VALUE, MAX_VALUE / 2.0));
	REQUIRE_FALSE(nearlyEqual(MAX_VALUE, -MAX_VALUE / 2.0));
	REQUIRE_FALSE(nearlyEqual(-MAX_VALUE, MAX_VALUE / 2.0));

	// Comparisons involving infinities
	constexpr powpp::Scalar POS_INFINITY = std::numeric_limits<powpp::Scalar>::infinity();
	constexpr powpp::Scalar NEG_INFINITY = -POS_INFINITY;
	REQUIRE(nearlyEqual(POS_INFINITY, POS_INFINITY));
	REQUIRE(nearlyEqual(NEG_INFINITY, NEG_INFINITY));
	REQUIRE_FALSE(nearlyEqual(NEG_INFINITY, POS_INFINITY));
	REQUIRE_FALSE(nearlyEqual(POS_INFINITY, MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(NEG_INFINITY, -MAX_VALUE));

	// Comparisons involving NaN
	constexpr powpp::Scalar NaN = std::numeric_limits<powpp::Scalar>::quiet_NaN();
	constexpr powpp::Scalar MIN_VALUE = std::numeric_limits<powpp::Scalar>::min();
	REQUIRE_FALSE(nearlyEqual(NaN, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, 0.0));
	REQUIRE_FALSE(nearlyEqual(0.0, NaN));
	REQUIRE_FALSE(nearlyEqual(-0.0, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, -0.0));
	REQUIRE_FALSE(nearlyEqual(NaN, POS_INFINITY));
	REQUIRE_FALSE(nearlyEqual(POS_INFINITY, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, NEG_INFINITY));
	REQUIRE_FALSE(nearlyEqual(NEG_INFINITY, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(MAX_VALUE, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, -MAX_VALUE));
	REQUIRE_FALSE(nearlyEqual(-MAX_VALUE, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, MIN_VALUE));
	REQUIRE_FALSE(nearlyEqual(MIN_VALUE, NaN));
	REQUIRE_FALSE(nearlyEqual(NaN, -MIN_VALUE));
	REQUIRE_FALSE(nearlyEqual(-MIN_VALUE, NaN));

	// Comparisons of numbers on opposite sides of 0
	REQUIRE_FALSE(nearlyEqual(1.000000001, -1.0));
	REQUIRE_FALSE(nearlyEqual(-1.0, 1.000000001));
	REQUIRE_FALSE(nearlyEqual(-1.000000001, 1.0));
	REQUIRE_FALSE(nearlyEqual(1.0, -1.000000001));
	REQUIRE_FALSE(nearlyEqual(10.0 * MIN_VALUE, 10.0 * -MIN_VALUE));
	REQUIRE_FALSE(nearlyEqual(10000.0 * MIN_VALUE, 10000.0 * -MIN_VALUE));

	// Comparisons of numbers very close to zero
	REQUIRE(nearlyEqual(MIN_VALUE, MIN_VALUE));
	//REQUIRE(nearlyEqual(MIN_VALUE, -MIN_VALUE));
	//REQUIRE(nearlyEqual(-MIN_VALUE, MIN_VALUE));
	//REQUIRE(nearlyEqual(MIN_VALUE, 0.0));
	//REQUIRE(nearlyEqual(0.0, MIN_VALUE));
	//REQUIRE(nearlyEqual(-MIN_VALUE, 0.0));
	//REQUIRE(nearlyEqual(0.0, -MIN_VALUE));

	REQUIRE_FALSE(nearlyEqual(0.000000001, MIN_VALUE));
	REQUIRE_FALSE(nearlyEqual(0.000000001, -MIN_VALUE));
	REQUIRE_FALSE(nearlyEqual(MIN_VALUE, 0.000000001));
	REQUIRE_FALSE(nearlyEqual(-MIN_VALUE, 0.000000001));
}

#pragma once

#include <chrono>
#include <list>

struct Result {
	std::chrono::microseconds duration;
	unsigned int iterations;
};

using Results = std::list<Result>;

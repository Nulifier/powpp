#pragma once

#include <type_traits>
#include <chrono>
#include <powpp/network.h>
#include <powpp/solver.h>
#include "types.h"

template <class T>
__attribute__((always_inline)) inline void DoNotOptimize(const T &value) {
	asm volatile("" : "+m"(const_cast<T &>(value)));	// NOLINT
}

template<class S>
Result runTest(powpp::Network(*makeNetwork)()) {
	static_assert(std::is_base_of<powpp::Solver, S>::value, "Template parameter S must be a solver");

	powpp::Network net = makeNetwork();

	net.compile();

	S solver(net);
	solver.tolerance = 1e-8;
	solver.maxIterations = 500;

	DoNotOptimize(net.getY());

	solver.solve();

	DoNotOptimize(solver.getSolveDuration());

	return {
		solver.getSolveDuration(),
		solver.getIterationCount()
	};
}

template<class S>
Results runTests(powpp::Network(*makeNetwork)(), std::chrono::milliseconds maxDuration) {
	using std::chrono::steady_clock;

	Results results;

	auto start = steady_clock::now();
	while ((steady_clock::now() - start) < maxDuration) {
		results.push_back(runTest<S>(makeNetwork));
	}

	return results;
}

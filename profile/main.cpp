#include <iomanip>
#include <ios>
#include <iostream>
#include <ostream>

#include "powpp/solver-jacobi.h"
#include "powpp/solver-jacobi-z.h"
#include "powpp/solver-gauss-seidel.h"
#include "powpp/solver-gauss-seidel-z.h"
#include "powpp/solver-newton-polar.h"
#include "powpp/solver-newton-rect.h"
#include "powpp/solver-fast-decoupled.h"
#include "runner.h"
#include "stats.h"
#include "test-models.h"

constexpr int MODEL_WIDTH = 20;
constexpr int SOLVER_WIDTH = 20;
constexpr int ITERS_WIDTH = 5;
constexpr int RUNS_WIDTH = 6;
constexpr int MIN_WIDTH = 4;
constexpr int MAX_WIDTH = 4;
constexpr int AVG_WIDTH = 10;
constexpr int STDDEV_WIDTH = 10;

struct RowHeader {
	RowHeader(std::string model, std::string solver)
	    : model(std::move(model)), solver(std::move(solver)) {}
	std::string model;
	std::string solver;
	friend std::ostream& operator<<(std::ostream& os, const RowHeader& h);
};

std::ostream& operator<<(std::ostream& os, const RowHeader& h) {
	os << std::left << "| " << std::setw(MODEL_WIDTH) << h.model << " | "
	   << std::setw(SOLVER_WIDTH) << h.solver << " | " << std::right;
	return os;
}

std::ostream& operator<<(std::ostream& os, const std::list<Result>& results) {
	const unsigned int iterations = results.front().iterations;

	os << std::setw(ITERS_WIDTH) << iterations << " | " << std::setw(RUNS_WIDTH)
	   << results.size() << " | " << std::setw(MIN_WIDTH) << getMin(results)
	   << " | " << std::setw(MAX_WIDTH) << getMax(results) << " | "
	   << std::setw(AVG_WIDTH) << getAvg(results) << " | "
	   << std::setw(STDDEV_WIDTH) << getStdDev(results) << " |";

	return os;
}

void printLine() {
	std::cout << std::setfill('-');
	std::cout << "*-" << std::setw(MODEL_WIDTH) << ""
	          << "-*-" << std::setw(SOLVER_WIDTH) << ""
	          << "-*-" << std::setw(ITERS_WIDTH) << ""
	          << "-*-" << std::setw(RUNS_WIDTH) << ""
	          << "-*-" << std::setw(MIN_WIDTH) << ""
	          << "-*-" << std::setw(MAX_WIDTH) << ""
	          << "-*-" << std::setw(AVG_WIDTH) << ""
	          << "-*-" << std::setw(STDDEV_WIDTH) << ""
	          << "-*";
	std::cout << std::setfill(' ') << std::endl;
}

void printHeader() {
	printLine();
	std::cout << std::left;
	std::cout << "| " << std::setw(MODEL_WIDTH) << "Model"
	          << " | " << std::setw(SOLVER_WIDTH) << "Solver"
	          << " | " << std::setw(ITERS_WIDTH) << "Iters"
	          << " | " << std::setw(RUNS_WIDTH) << "Runs"
	          << " | " << std::setw(MIN_WIDTH) << "Min"
	          << " | " << std::setw(MAX_WIDTH) << "Max"
	          << " | " << std::setw(AVG_WIDTH) << "Avg"
	          << " | " << std::setw(STDDEV_WIDTH) << "Std Dev"
	          << " |";
	std::cout << std::right << std::endl;
	printLine();
}

int main() {
	using namespace std::chrono_literals;

	const auto maxDuration = 500ms;

	auto lynnPowellNoGen = []() {
		return models::lynnPowellWithoutGenerator();
	};

	auto lynnPowellGen = []() {
		return models::lynnPowellWithGenerator();
	};

	auto ieee14 = []() {
		return models::ieee14();
	};

	// Print header
	printHeader();

	Results results;

	results = runTests<powpp::SolverJacobi>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Jacobi") << results
	          << std::endl;

	results = runTests<powpp::SolverGaussSeidel>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Gauss Seidel") << results
	          << std::endl;

	results = runTests<powpp::SolverJacobiZ>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Jacobi Z") << results
	          << std::endl;
	
	results = runTests<powpp::SolverGaussSeidelZ>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Gauss Seidel Z") << results
	          << std::endl;

	results = runTests<powpp::SolverNewtonRect>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Newton Rect") << results
	          << std::endl;

	results = runTests<powpp::SolverNewtonPolar>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Newton Polar") << results
	          << std::endl;

	results = runTests<powpp::SolverFastDecoupled>(lynnPowellNoGen, maxDuration);
	std::cout << RowHeader("LynnPowell No Gen", "Fast Decoupled") << results
	          << std::endl;

	printLine();

	results = runTests<powpp::SolverJacobi>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Jacobi") << results
	          << std::endl;

	results = runTests<powpp::SolverGaussSeidel>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Gauss Seidel") << results
	          << std::endl;

	results = runTests<powpp::SolverJacobiZ>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Jacobi Z") << results
	          << std::endl;
	
	results = runTests<powpp::SolverGaussSeidelZ>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Gauss Seidel Z") << results
	          << std::endl;

	results = runTests<powpp::SolverNewtonPolar>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Newton Polar") << results
	          << std::endl;

	results = runTests<powpp::SolverFastDecoupled>(lynnPowellGen, maxDuration);
	std::cout << RowHeader("LynnPowell Gen", "Fast Decoupled") << results
	          << std::endl;

	printLine();

	results = runTests<powpp::SolverJacobi>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Jacobi") << results
	          << std::endl;

	results = runTests<powpp::SolverGaussSeidel>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Gauss Seidel") << results
	          << std::endl;

	results = runTests<powpp::SolverJacobiZ>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Jacobi Z") << results
	          << std::endl;

	results = runTests<powpp::SolverGaussSeidelZ>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Gauss Seidel Z") << results
	          << std::endl;
	
	results = runTests<powpp::SolverNewtonPolar>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Newton Polar") << results
	          << std::endl;

	results = runTests<powpp::SolverFastDecoupled>(ieee14, maxDuration);
	std::cout << RowHeader("IEEE 14 Bus", "Fast Decoupled") << results
	          << std::endl;

	printLine();

	return 0;
}

#pragma once

#include <chrono>
#include <cmath>
#include <limits>
#include <ratio>
#include "powpp/types.h"
#include "types.h"

inline powpp::Scalar getMin(const Results& results) {
	powpp::Scalar min = std::numeric_limits<powpp::Scalar>::infinity();
	for (const auto& result : results) {
		auto microseconds = std::chrono::duration<powpp::Scalar, std::micro>(result.duration);
		if (microseconds.count() < min) {
			min = microseconds.count();
		}
	}
	return min;
}

inline powpp::Scalar getMax(const Results& results) {
	powpp::Scalar max = 0.0;
	for (const auto& result : results) {
		auto microseconds = std::chrono::duration<powpp::Scalar, std::micro>(result.duration);
		if (microseconds.count() > max) {
			max = microseconds.count();
		}
	}
	return max;
}

inline powpp::Scalar getAvg(const Results& results) {
	powpp::Scalar avg = 0.0;
	for (const auto& result : results) {
		avg += std::chrono::duration<powpp::Scalar, std::micro>(result.duration).count();
	}
	return avg / static_cast<powpp::Scalar>(results.size());
}

inline powpp::Scalar getStdDev(const Results& results) {
	const powpp::Scalar avg = getAvg(results);

	powpp::Scalar sum = 0.0;
	for (const auto& result : results) {
		sum += std::chrono::duration<powpp::Scalar, std::micro>(result.duration).count() - avg;
	}
	return std::sqrt(avg / static_cast<powpp::Scalar>(results.size()));
}

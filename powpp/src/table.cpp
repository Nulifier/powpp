#include "powpp/table.h"
#include <array>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>

std::string powpp::Table::str() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

void powpp::Table::print() const { std::cout << *this << std::endl; }

std::string powpp::Table::toString(Scalar value) const {
	std::array<char, SCALAR_LEN> buf;

	(void)std::snprintf(buf.data(), buf.size(), "%f", value);

	return {buf.data()};
}

std::string powpp::Table::toString(Complex value) const {
	std::array<char, SCALAR_LEN * 2 + 2> buf;

	(void)std::snprintf(buf.data(), buf.size(), "%f%s%f%s", value.real(),
	                    value.imag() < 0 ? "-" : "+", std::abs(value.imag()),
	                    format.complexJ ? "j" : "i");

	return {buf.data()};
}

std::ostream& powpp::operator<<(std::ostream& os, const powpp::Table& t) {
	using powpp::Table;

	// Print header

	// Print rows
	for (size_t rowIdx = 0; rowIdx < t.rows.size(); ++rowIdx) {
		const Table::Row& row = t.rows[rowIdx];
		if (rowIdx != 0) {
			os << std::endl;
		}

		for (size_t colIdx = 0; colIdx < row.data.size(); ++colIdx) {
			if (colIdx != 0) {
				os << " ";
			}

			os << std::setw(t.cols[colIdx].maxWidth) << row.data[colIdx];
		}
	}

	return os;
}

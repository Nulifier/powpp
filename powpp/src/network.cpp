#include "powpp/network.h"
#include "powpp/eigen.h"
#include "powpp/types.h"
#include <cassert>
#include <powpp/math.h>
#include <stdexcept>

powpp::BusId powpp::Network::selectSlackBus() const {
	// Select the bus with the largest real power
	BusId slack = buses.cbegin()->first;
	Scalar maxPower = 0.0;

	for (auto [id, gen] : generators) {
		if (gen.realPower > maxPower) {
			slack = gen.bus;
			maxPower = gen.realPower;
		}
	}

	return slack;
}

powpp::Scalar powpp::Network::getMaxRealPower() const {
	Scalar max = 0.0;

	for (auto [id, load] : loads) {
		Scalar loadPower = std::abs(load.power.real());
		if (loadPower > max) {
			max = loadPower;
		}
	}

	for (auto [id, gen] : generators) {
		Scalar genPower = std::abs(gen.realPower);
		if (genPower > max) {
			max = genPower;
		}
	}

	return max;
}

bool powpp::Network::isValid() const {
	// Verify that the lines connections are valid
	for (auto [id, line] : lines) {
		// Verify the line doesn't connect to itself
		if (line.busA == line.busB) {
			return false;
		}

		// Verify the BusIds exist
		if (!isBusIdValid(line.busA) || !isBusIdValid(line.busB)) {
			return false;
		}

		const Bus& a = buses.at(line.busA);
		const Bus& b = buses.at(line.busB);

		// Verify the bus voltages are compatible
		if (!math::nearlyEqual(a.nominalVoltage, b.nominalVoltage)) {
			return false;
		}
	}

	return true;
}

void powpp::Network::compile(BusId slackBus, Scalar SbaseOverride) {
	const int n = static_cast<int>(buses.size());

	// Build the index of BusId to index
	indexLookup.clear();
	indexLookup.reserve(n);
	Eigen::Index index = 0;
	for (auto [id, bus] : buses) {
		indexLookup[id] = index++;
	}

	Sbase = SbaseOverride;

	Y.resize(n, n);
	busTypes.clear();
	busTypes.reserve(n);

	connectedPower.setZero(n);
	initalVoltageMagnitudes.setConstant(n, 1.0);

	for (auto [id, gen] : generators) {
		// Mark all buses with generators as PV
		busTypes[gen.bus] = BusType::PV;

		// Generators are positive power
		connectedPower[getBusIndex(gen.bus)] += gen.realPower;

		// Set the initial voltage
		initalVoltageMagnitudes[getBusIndex(gen.bus)] = gen.voltageSetPoint;
	}

	for (auto [id, load] : loads) {
		// Loads are negative power
		connectedPower[getBusIndex(load.bus)] -= load.power;
	}

	// Convert connected power from real units to pu
	connectedPower = connectedPower / Sbase;

	// Assign the slack bus
	busTypes[slackBus] = BusType::PD;

	// Add in shunt admittances
	for (auto [id, shunt] : shunts) {
		const auto idx = indexLookup.at(shunt.bus);
		Y.coeffRef(idx, idx) += math::impedanceToAdmittance(shunt.impedance);
	}

	// Add in line admittances
	for (auto [id, line] : lines) {
		const Index idxA = getBusIndex(line.busA);
		const Index idxB = getBusIndex(line.busB);

		// The voltages should be the same at each bus
		const Scalar Vbase = buses.at(line.busA).nominalVoltage;
		const Scalar Zbase = Vbase * Vbase / Sbase;

		// π model: sum of impedance from resistance and inductance
		const Complex y =
		    Zbase * math::impedanceToAdmittance(line.getImpedance());
		// π model: half of the capacitance (each end of the line gets half)
		const Complex ys =
		    Zbase * line.getShuntAdmittance() / Complex(2.0, 0.0);

		// Diagonal elements
		Y.coeffRef(idxA, idxA) += y + ys;
		Y.coeffRef(idxB, idxB) += y + ys;

		// Off-diagonal elements
		Y.coeffRef(idxA, idxB) += -y;
		Y.coeffRef(idxB, idxA) += -y;
	}

	// Add in transformer admittances
	for (auto [id, transformer] : transformers) {
		const Index idxHV = getBusIndex(transformer.busHV);
		const Index idxLV = getBusIndex(transformer.busLV);

		// These value are already in p.u.
		const Complex Yleak =
		    math::impedanceToAdmittance(transformer.leakageImpedance);
		const Complex Ymag =
		    math::impedanceToAdmittance(transformer.magnetizingImpedance);

		const Complex tap =
		    Complex(transformer.tapPosition * std::cos(transformer.phaseShift),
		            transformer.tapPosition * std::sin(transformer.phaseShift));

		// primary to primary
		Y.coeffRef(idxHV, idxHV) += (Yleak + Ymag) / (tap * tap);
		// primary to secondary
		Y.coeffRef(idxHV, idxLV) += -(Yleak + Ymag) / std::conj(tap);
		// secondary to primary
		Y.coeffRef(idxLV, idxHV) +=
		    -Yleak / Complex(transformer.tapPosition, 0.0);
		// secondary to secondary
		Y.coeffRef(idxLV, idxLV) += Yleak;
	}

	// Most algorithms need this matrix to be compressed anyways
	Y.makeCompressed();

	Z.resize(0, 0);
	Zred.resize(0, 0);

	// Built the index lists
	pdIndices.clear();
	pvIndices.clear();
	pqIndices.clear();
	for (auto [id, bus] : buses) {
		BusType type = getBusType(id);
		if (type == BusType::PD) {
			pdIndices.push_back(getBusIndex(id));
		}
		else if (type == BusType::PV) {
			pvIndices.push_back(getBusIndex(id));
		}
		else if (type == BusType::PQ) {
			pqIndices.push_back(getBusIndex(id));
		}
	}

	pvpqIndices.clear();
	pvpqIndices.reserve(pvIndices.size() + pqIndices.size());
	pvpqIndices.insert(pvpqIndices.end(), pvIndices.begin(), pvIndices.end());
	pvpqIndices.insert(pvpqIndices.end(), pqIndices.begin(), pqIndices.end());

	assert(pdIndices.size() + pvIndices.size() + pqIndices.size() ==
	       buses.size());
}

powpp::Network::BusType powpp::Network::getBusType(BusId id) const {
	auto itr = busTypes.find(id);
	if (itr != busTypes.end()) {
		return itr->second;
	}
	return BusType::PQ;
}

void powpp::Network::updateZ() const {
	throw std::logic_error("Z matrix has not been implemented");
}

void powpp::Network::updateZred() const {
	// Copy the Y matrix
	Zred = Y;

	// Remove all the slack bus rows and columns
	for (Index idx : pdIndices) {
		eigen::removeCross(Zred, idx, idx);
	}

	/// @TODO Figure out how to make this invert in-place.
	Zred = Zred.inverse();

	// Add back in all the slack buses as zeros
	for (Index idx : pdIndices) {
		eigen::expandCross(Zred, idx, idx);
	}
}

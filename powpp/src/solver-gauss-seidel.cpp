#include "powpp/solver-gauss-seidel.h"
#include "powpp/eigen.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"

powpp::SolverGaussSeidel::SolverGaussSeidel(const Network& network,
                                            Scalar accelFactor)
    : network(network), accelFactor(accelFactor),
      sol(VecCx::Constant(static_cast<Index>(network.buses.size()),
                          DEFAULT_VOLTAGE),
          network.getConnectedPower()) {}

bool powpp::SolverGaussSeidel::canSolve() const {
	return network.nPD() == 1;
}

powpp::Solver::Result powpp::SolverGaussSeidel::doSolve() {
	assert(canSolve());

	/// Number of buses (including slack)
	const Index n = static_cast<Index>(network.buses.size());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];

	const SpMatCx& Y = network.getY();

	VecCx& V = sol.V;
	VecCx& S = sol.S;

	while (!converged && ((iterations++) < maxIterations)) {
		Scalar maxDiff = 0.0;

		for (Index k = 0; k < n; ++k) {
			// Skip the slack bus
			if (k == slackBus) {
				continue;
			}

			Complex Iinj = std::conj(S[k] / V[k]);
			const auto vec = V.cwiseProduct(Y.innerVector(k));
			Complex Iline;
			if (k != 0) {
				Iline += vec.head(k).sum();
			}
			if (k != n - 1) {
				Iline += vec.tail(n - k - 1).sum();
			}

			Complex Vnew = (Iinj - Iline) / Y.coeff(k, k);
			Scalar Vdiff = std::abs(Vnew - V[k]);

			V[k] = accelFactor * Vnew - (accelFactor - 1) * V[k];

			if (Vdiff > maxDiff) {
				maxDiff = Vdiff;
			}
		}

		if (maxDiff < tolerance) {
			converged = true;
		}
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

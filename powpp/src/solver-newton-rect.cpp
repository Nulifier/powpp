#include "powpp/solver-newton-rect.h"
#include "powpp/eigen.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"
#include <Eigen/src/Core/util/Constants.h>
#include <iostream>
#include <string>

template <typename Derived>
void printMat(const std::string& header, const Eigen::EigenBase<Derived>& obj) {
	powpp::Table table;
	table.setData(obj);
	std::cout << header << std::endl;
	table.print();
}

powpp::SolverNewtonRect::SolverNewtonRect(const Network& network)
    : network(network),
      sol(VecCx::Constant(static_cast<Index>(network.buses.size()),
                          DEFAULT_VOLTAGE),
          network.getConnectedPower()) {}

bool powpp::SolverNewtonRect::canSolve() const { return network.nPD() == 1; }

powpp::Solver::Result powpp::SolverNewtonRect::doSolve() {
	assert(canSolve());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];

	const Indices& pvpq = network.PVPQ();
	const Indices& pv = network.PV();
	const Indices& pq = network.PQ();

	Index nPV = static_cast<Index>(network.PV().size());
	Index nPQ = static_cast<Index>(network.PQ().size());
	Index nPVPQ = nPV + nPQ;

	const SpMatCx& Y = network.getY();
	const VecCx& Ssp = network.getConnectedPower();
	const Vec V2sp = sol.V.cwiseAbs2();

	VecCx& V = sol.V;
	VecCx& S = sol.S;

	Mat J = Mat::Zero(2 * nPVPQ, 2 * nPVPQ);
	Vec y(2 * nPVPQ);

	while (!converged && ((iterations++) < maxIterations)) {
		VecCx deltaS = Ssp - (V.conjugate().cwiseProduct(Y * V)).conjugate();

		// Check if we have converged
		if ((deltaS(pvpq).real().array().abs() <= tolerance).all() &&
		    (deltaS(pvpq).imag().array().abs() <= tolerance).all()) {
			converged = true;
			break;
		}

		jacobian(J, Y, V);

		y << deltaS(pvpq).real(), deltaS(pq).imag(),
		    V2sp(pv) - V(pv).cwiseAbs2();

		Eigen::FullPivLU<Mat> lu(J);
		auto dV = lu.solve(y);

		V(pvpq).real() += dV.topRows(nPVPQ);       // dE
		V(pq).imag() += dV.middleRows(nPVPQ, nPQ); // dF(pq)
		V(pv).imag() += dV.bottomRows(nPV);        // dF(pv)

		// std::cout << "Iteration " << iterations - 1 << std::endl;
		// printMat("dS", deltaS.bottomRows(nPVPQ));
		// printMat("dV", dV.eval());
		// printMat("V", V);
		// std::cout << std::endl;

		// TODO: Handle generators
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

void powpp::SolverNewtonRect::jacobian(Mat& J, const SpMatCx& Y,
                                       const VecCx& V) {

	using namespace std::complex_literals;
	
	const Indices& pvpq = network.PVPQ();
	const Indices& pv = network.PV();
	const Indices& pq = network.PQ();

	Index nPVPQ = network.nPVPQ();
	Index nPV = network.nPV();
	Index nPQ = network.nPQ();

	auto J1 = J.topLeftCorner(nPVPQ, nPVPQ);
	auto J2 = J.topRightCorner(nPVPQ, nPVPQ);
	auto J3 = J.block(nPVPQ, 0, nPQ, nPVPQ);
	auto J4 = J.block(nPVPQ, nPVPQ, nPQ, nPVPQ);
	auto J5 = J.bottomLeftCorner(nPV, nPVPQ);
	auto J6 = J.bottomRightCorner(nPV, nPVPQ);

	//       dE
	//       dF
	// J1 J2 dP
	// J3 J4 dQ

	MatCx diagI = (Y * V).asDiagonal();

	MatCx VY = V.asDiagonal() * Y.conjugate();

	auto dS_dVr = diagI.conjugate() + VY;
	auto dS_dVi = 1i * (diagI.conjugate() - VY);

	J1 = dS_dVr(pvpq, pvpq).real();
	J2 = dS_dVi(pvpq, pvpq).real();
	J3 = dS_dVr(pq, pvpq).imag();
	J4 = dS_dVi(pq, pvpq).imag();
	J5(pv, pv) = V(pv).real().asDiagonal();
	J6(pv, pv) = V(pv).imag().asDiagonal();
}

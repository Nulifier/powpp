#include "powpp/solver.h"
#include <chrono>

powpp::Solver::Result powpp::Solver::solve() {
	using Clock = std::chrono::steady_clock;

	Clock::time_point start = Clock::now();
	Result result = doSolve();
	Clock::time_point end = Clock::now();
	solveDuration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	assert(iterationCount != 0 && "Remember to call setIterationCound() in doSolve()");
	return result;
}

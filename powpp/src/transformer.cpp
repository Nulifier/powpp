#include "powpp/transformer.h"
#include "powpp/types.h"

powpp::Transformer::Transformer(BusId busHV, BusId busLV,
                                const TransformerType& type)
    : busHV(busHV), busLV(busLV), phaseShift(type.phaseShift),
      leakageImpedance(type.leakageImpedance),
      magnetizingImpedance(type.magnetizingImpedance) {}

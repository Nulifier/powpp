#include "powpp/solver-jacobi-z.h"
#include "powpp/eigen.h"
#include "powpp/network.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"
#include <cassert>

powpp::SolverJacobiZ::SolverJacobiZ(const Network& network)
    : network(network),
      sol(VecCx::Constant(static_cast<Index>(network.buses.size()),
                          DEFAULT_VOLTAGE),
          network.getConnectedPower()) {}

bool powpp::SolverJacobiZ::canSolve() const {
	return network.nPD() == 1;
}

powpp::Solver::Result powpp::SolverJacobiZ::doSolve() {
	assert(canSolve());

	/// Number of buses (including slack)
	const Index n = static_cast<Index>(network.buses.size());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];
	const auto& pvIndices = network.PV();
	const auto& pvpqIndices = network.PVPQ();

	const auto& Y = network.getY();
	const auto& Z = network.getZred();

	VecCx& V = sol.V;
	VecCx& S = sol.S;

	// Calculate C
	const VecCx C = Z * Y.innerVector(slackBus) * V(slackBus);

	while (!converged && ((iterations++) < maxIterations)) {
		// Calculate current from generators and loads
		VecCx I = (S.array() / V.array()).conjugate().matrix();

		// Calculate new voltages
		VecCx Vnew = Z * I - C;

		// Scale the PV voltages
		Vnew(pvIndices).array() =
		    Vnew(pvIndices).array() *
		    (V(pvIndices).array().abs() / Vnew(pvIndices).array().abs());

		// Calculate the maximum absolute difference
		Scalar Vdiff = (Vnew - V)(pvpqIndices).cwiseAbs().maxCoeff();

		// Update the voltage
		V(pvpqIndices) = Vnew(pvpqIndices);

		// Check if the maximum absolute difference is
		if (Vdiff < tolerance) {
			converged = true;
		}
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

#include "powpp/solver-newton-polar.h"
#include "powpp/eigen.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"
#include <Eigen/src/Core/util/Constants.h>
#include <iostream>
#include <string>

template <typename Derived>
void printMat(const std::string& header, const Eigen::EigenBase<Derived>& obj) {
	powpp::Table table;
	table.setData(obj);
	std::cout << header << std::endl;
	table.print();
}

powpp::SolverNewtonPolar::SolverNewtonPolar(const Network& network)
    : network(network),
      sol(network.getInitialVoltageMagnitudes(), Vec::Zero(network.n()),
          network.getConnectedPower()) { }

bool powpp::SolverNewtonPolar::canSolve() const { return network.nPD() == 1; }

powpp::Solver::Result powpp::SolverNewtonPolar::doSolve() {
	assert(canSolve());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];

	const Indices& pvpq = network.PVPQ();
	const Indices& pv = network.PV();
	const Indices& pq = network.PQ();

	Index n = network.n();
	Index nPV = network.nPV();
	Index nPQ = network.nPQ();
	Index nPVPQ = nPV + nPQ;

	const SpMatCx& Y = network.getY();
	const VecCx& Ssp = network.getConnectedPower();

	VecCx& S = sol.S;
	Vec& Vmag = sol.Vmag;
	Vec& Varg = sol.Varg;

	Mat J = Mat::Zero(nPVPQ + nPQ, nPVPQ + nPQ);
	Vec y(nPVPQ + nPQ);

	while (!converged && ((iterations++) < maxIterations)) {
		Vec dP = Vec::Zero(n);
		for (Index k : pvpq) {
			Scalar val = 0.0;
			for (Index j = 0; j < n; ++j) {
				val += Vmag(j) *
				       (Y.coeff(k, j).real() * std::cos(Varg(k) - Varg(j)) +
				        Y.coeff(k, j).imag() * std::sin(Varg(k) - Varg(j)));
			}
			dP(k) = Ssp(k).real() - val * Vmag(k);
		}
		Vec dQ = Vec::Zero(n);
		for (Index k : pq) {
			Scalar val = 0.0;
			for (Index j = 0; j < n; ++j) {
				val += Vmag(j) *
				       (Y.coeff(k, j).real() * std::sin(Varg(k) - Varg(j)) -
				        Y.coeff(k, j).imag() * std::cos(Varg(k) - Varg(j)));
			}
			dQ(k) = Ssp(k).imag() - val * Vmag(k);
		}

		// Check if we have converged
		if ((dP(pvpq).array().abs() <= tolerance).all() &&
		    (dQ(pq).array().abs() <= tolerance).all()) {
			converged = true;
			break;
		}

		jacobian(J, Y, Vmag, Varg);

		y << dP(pvpq), dQ(pq);

		Eigen::FullPivLU<Mat> lu(J);
		auto dV = lu.solve(y);

		// Update voltage phase
		Varg(pvpq).array() += dV.topRows(nPVPQ).array();
		// Update voltage magnitude
		Vmag(pq).array() = Vmag(pq).array() * (dV.bottomRows(nPQ).array() + 1.0);

		// TODO: Handle generators

		// std::cout << "Iteration " << iterations - 1 << std::endl;
		// printMat("dP", dP);
		// printMat("dQ", dQ);
		// printMat("J", J.eval());
		// printMat("dVarg", dV.topRows(nPVPQ).eval());
		// printMat("dVmag", dV.bottomRows(nPQ).eval());
		// printMat("Varg", Varg);
		// printMat("Vmag", Vmag);
		//   std::cout << std::endl;
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

void powpp::SolverNewtonPolar::jacobian(Mat& J, const SpMatCx& Y,
                                        const Vec& Vmag, const Vec& Varg) {

	using namespace std::complex_literals;

	const Indices& pvpq = network.PVPQ();
	const Indices& pv = network.PV();
	const Indices& pq = network.PQ();

	Index nPVPQ = network.nPVPQ();
	Index nPV = network.nPV();
	Index nPQ = network.nPQ();

	auto J1 = J.topLeftCorner(nPVPQ, nPVPQ);
	auto J2 = J.topRightCorner(nPVPQ, nPQ);
	auto J3 = J.bottomLeftCorner(nPQ, nPVPQ);
	auto J4 = J.bottomRightCorner(nPQ, nPQ);

	//       dE
	//       dF
	// J1 J2 dP
	// J3 J4 dQ

	MatCx V = (Vmag.cast<Complex>().array() * (1.0i * Varg.cast<Complex>()).array().exp()).matrix();

	MatCx diagV = V.asDiagonal();
	MatCx diagI = (Y * V).asDiagonal();
	MatCx diagVnorm = V.cwiseQuotient(V.cwiseAbs()).asDiagonal();

	auto dS_dVm =
	    diagV * (Y * diagVnorm).conjugate() + diagI.conjugate() * diagVnorm;
	auto dS_dVa = 1.0i * diagV * (diagI - Y * diagV).conjugate();

	J1 = dS_dVa(pvpq, pvpq).real();
	J2 = dS_dVm(pvpq, pq).real();
	J3 = dS_dVa(pq, pvpq).imag();
	J4 = dS_dVm(pq, pq).imag();
}

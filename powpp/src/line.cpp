#include "powpp/line.h"
#include "powpp/types.h"
#include <complex>

powpp::Line::Line(BusId busA, BusId busB, const LineType& type, Scalar length): busA(busA), busB(busB), length(length) {
	setLineType(type);
}

powpp::Line::Line(BusId busA, BusId busB, Scalar r, Scalar x, Scalar b, Scalar length): busA(busA), busB(busB), length(length) {
	impedance = std::complex(r, x) * length;
	shuntAdmittance = std::complex(0.0, b) * length;
}

void powpp::Line::setLineType(const LineType &type) {
	impedance = type.impedance * length;
	shuntAdmittance = type.shuntAdmittance * length;
}

void powpp::Line::setLength(Scalar newLength) {
	const Complex impedancePerKm = impedance / length;
	const Complex admittancePerKm = shuntAdmittance / length;
	length = newLength;
	impedance = impedancePerKm * length;
	shuntAdmittance = admittancePerKm * length;
}

#include "powpp/solver-gauss-seidel-z.h"
#include "powpp/eigen.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"

powpp::SolverGaussSeidelZ::SolverGaussSeidelZ(const Network& network,
                                              Scalar accelFactor)
    : network(network), accelFactor(accelFactor),
      sol(VecCx::Constant(static_cast<Index>(network.buses.size()),
                          DEFAULT_VOLTAGE),
          network.getConnectedPower())

{}

bool powpp::SolverGaussSeidelZ::canSolve() const {
	return network.nPD() == 1;
}

powpp::Solver::Result powpp::SolverGaussSeidelZ::doSolve() {
	assert(canSolve());

	/// Number of buses (including slack)
	const Index n = static_cast<Index>(network.buses.size());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];
	const auto& pvIndices = network.PV();
	const auto& pvpqIndices = network.PVPQ();

	const SpMatCx& Y = network.getY();
	const MatCx& Z = network.getZred();

	VecCx& V = sol.V;
	VecCx& S = sol.S;

	// Calculate C
	const VecCx C = Z * Y.innerVector(slackBus) * V(slackBus);

	while (!converged && ((iterations++) < maxIterations)) {
		Scalar maxDiff = 0.0;

		VecCx I = (S.array() / V.array()).conjugate().matrix();

		for (Index k = 0; k < n; ++k) {
			// Skip the slack bus
			if (k == slackBus) {
				continue;
			}

			Complex Vnew = (Z.innerVector(k).array() * I.array()).sum() - C[k];
			Scalar Vdiff = std::abs(Vnew - V[k]);

			// Update voltage
			V[k] = accelFactor * Vnew - (accelFactor - 1) * V[k];

			// Update current
			I[k] = std::conj(S[k] / V[k]);

			if (Vdiff > maxDiff) {
				maxDiff = Vdiff;
			}
		}

		if (maxDiff < tolerance) {
			converged = true;
		}
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

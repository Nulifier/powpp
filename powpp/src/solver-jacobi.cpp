#include "powpp/solver-jacobi.h"
#include "powpp/eigen.h"
#include "powpp/network.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"
#include <cassert>

powpp::SolverJacobi::SolverJacobi(const Network& network)
    : network(network),
      sol(VecCx::Constant(static_cast<Index>(network.buses.size()),
                          DEFAULT_VOLTAGE),
          network.getConnectedPower()) {}

bool powpp::SolverJacobi::canSolve() const {
	return network.nPD() == 1;
}

powpp::Solver::Result powpp::SolverJacobi::doSolve() {
	assert(canSolve());

	/// Number of buses (including slack)
	const Index n = static_cast<Index>(network.buses.size());

	bool converged = false;
	unsigned int iterations = 0;

	const auto& pvIndices = network.PV();
	const auto& pvpqIndices = network.PVPQ();

	const auto& Y = network.getY();

	VecCx& V = sol.V;
	VecCx& S = sol.S;

	while (!converged && ((iterations++) < maxIterations)) {
		// Calculate current from generators and loads
		auto Iinj = (S.array() / V.array()).conjugate().matrix();

		VecCx Iline = VecCx::Zero(n);
		for (Index i = 0; i < n; ++i) {
			const auto vec = V.cwiseProduct(Y.innerVector(i));
			// Elements before i
			if (i != 0) {
				Iline[i] += vec.head(i).sum();
			}
			// Elements after i
			if (i != n - 1) {
				Iline[i] += vec.tail(n - i - 1).sum();
			}
		}

		// Calculate new voltages
		VecCx Vnew = Y.diagonal().cwiseInverse().cwiseProduct(Iinj - Iline);

		// Scale the PV voltages
		Vnew(pvIndices).array() =
		    Vnew(pvIndices).array() *
		    (V(pvIndices).array().abs() / Vnew(pvIndices).array().abs());

		// Calculate the maximum absolute difference
		Scalar Vdiff = (Vnew - V)(pvpqIndices).cwiseAbs().maxCoeff();

		// Update the voltage
		V(pvpqIndices) = Vnew(pvpqIndices);

		// Check if the maximum absolute difference is
		if (Vdiff < tolerance) {
			converged = true;
		}
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

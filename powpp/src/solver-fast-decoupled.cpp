#include "powpp/solver-fast-decoupled.h"
#include "powpp/eigen.h"
#include "powpp/solver.h"
#include "powpp/table.h"
#include "powpp/types.h"
#include <Eigen/src/Core/util/Constants.h>
#include <Eigen/src/LU/FullPivLU.h>
#include <iostream>
#include <string>

template <typename Derived>
void printMat(const std::string& header, const Eigen::EigenBase<Derived>& obj) {
	powpp::Table table;
	table.setData(obj);
	std::cout << header << std::endl;
	table.print();
}

powpp::SolverFastDecoupled::SolverFastDecoupled(const Network& network)
    : network(network),
      sol(network.getInitialVoltageMagnitudes(), Vec::Zero(network.n()),
          network.getConnectedPower()) { }

bool powpp::SolverFastDecoupled::canSolve() const { return network.nPD() == 1; }

powpp::Solver::Result powpp::SolverFastDecoupled::doSolve() {
	assert(canSolve());

	bool converged = false;
	unsigned int iterations = 0;

	Index slackBus = network.PD()[0];

	const Indices& pvpq = network.PVPQ();
	const Indices& pv = network.PV();
	const Indices& pq = network.PQ();

	Index n = network.n();
	Index nPV = network.nPV();
	Index nPQ = network.nPQ();
	Index nPVPQ = nPV + nPQ;

	const SpMatCx& Y = network.getY();
	const VecCx& Ssp = network.getConnectedPower();

	VecCx& S = sol.S;
	Vec& Vmag = sol.Vmag;
	Vec& Varg = sol.Varg;

	Mat J1 = Mat::Zero(nPVPQ, nPVPQ);
	Mat J4 = Mat::Zero(nPQ, nPQ);

	jacobian(J1, J4, Y);
	Eigen::FullPivLU J1lu = J1.fullPivLu();
	Eigen::FullPivLU J4lu = J4.fullPivLu();

	while (!converged && ((iterations++) < maxIterations)) {
		Vec dP = Vec::Zero(n);
		for (Index k : pvpq) {
			Scalar val = 0.0;
			for (Index j = 0; j < n; ++j) {
				val += Vmag(j) *
				       (Y.coeff(k, j).real() * std::cos(Varg(k) - Varg(j)) +
				        Y.coeff(k, j).imag() * std::sin(Varg(k) - Varg(j)));
			}
			dP(k) = Ssp(k).real() - val * Vmag(k);
		}
		Vec dQ = Vec::Zero(n);
		for (Index k : pq) {
			Scalar val = 0.0;
			for (Index j = 0; j < n; ++j) {
				val += Vmag(j) *
				       (Y.coeff(k, j).real() * std::sin(Varg(k) - Varg(j)) -
				        Y.coeff(k, j).imag() * std::cos(Varg(k) - Varg(j)));
			}
			dQ(k) = Ssp(k).imag() - val * Vmag(k);
		}

		// Check if we have converged
		if ((dP(pvpq).array().abs() <= tolerance).all() &&
		    (dQ(pq).array().abs() <= tolerance).all()) {
			converged = true;
			break;
		}

		// Solve the equations
		Vec dVarg = J1lu.solve(dP(pvpq));
		Vec dVmag = J4lu.solve(dQ(pq));

		// Update voltage phase
		Varg(pvpq).array() += dVarg.array();
		// Update voltage magnitude
		Vmag(pq).array() = Vmag(pq).array() * (dVmag.array() + 1.0);

		// TODO: Handle generators

		//std::cout << "Iteration " << iterations - 1 << std::endl;
		//printMat("dP", dP);
		//printMat("dQ", dQ);
		//printMat("J1", J1.eval());
		//printMat("J4", J4.eval());
		//printMat("dVarg", dVarg);
		//printMat("dVmag", dVmag);
		//printMat("Varg", Varg);
		//printMat("Vmag", Vmag);
		//   std::cout << std::endl;
	}

	setIterationCount(iterations - 1);

	return converged ? Result::Solved : Result::NotSolved;
}

void powpp::SolverFastDecoupled::jacobian(Mat& J1, Mat& J4, const SpMatCx& Y) {
	const Indices& pvpq = network.PVPQ();
	const Indices& pq = network.PQ();

	Index nPVPQ = network.nPVPQ();
	Index nPQ = network.nPQ();

	//       dE
	//       dF
	// J1  0 dP
	//  0 J4 dQ

	// Calculate J1: Susceptances of unknown δs for PV and PQ buses
	for (Index j = 0; j < nPVPQ; ++j) {
		const auto col = pvpq[j];
		const auto& y = Y.innerVector(col);
		for (Index i = 0; i < nPVPQ; ++i) {
			const auto row = pvpq[i];
			J1(j, i) = - y.coeff(row).imag();
		}
	}

	// Calculate J4: Susceptances of unknown |V| for PQ buses
	for (Index j = 0; j < nPQ; ++j) {
		const auto col = pq[j];
		const auto& y = Y.innerVector(col);
		for (Index i = 0; i < nPQ; ++i) {
			const auto row = pq[i];
			J4(j, i) = - y.coeff(row).imag();
		}
	}
}

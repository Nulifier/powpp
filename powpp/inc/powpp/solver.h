#pragma once

#include "powpp/types.h"
#include <chrono>

namespace powpp {
	class Solver {
	public:
		enum class Result {
			Solved,   //< The problem converged.
			NotSolved //< The problem did not converge in the maxIterations.
		};

		static constexpr Complex DEFAULT_VOLTAGE{ 1.0, 0.0 };

		virtual ~Solver() = default;

		/// The allowable deviation in the solution to finish solving.
		Scalar tolerance = 1e-9;

		/// The maximum number of iterations that a solver will attempt.
		unsigned int maxIterations = 100;

		virtual bool canSolve() const = 0;

		Result solve();

		std::chrono::microseconds getSolveDuration() const { return solveDuration; }

		unsigned int getIterationCount() const { return iterationCount; }

	protected:
		virtual Result doSolve() = 0;

		/// Call when iteration is done with the number of iterations performed.
		void setIterationCount(unsigned int count) { iterationCount = count; }

	private:
		std::chrono::microseconds solveDuration;
		unsigned int iterationCount = 0;
	};
}

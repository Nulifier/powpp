#pragma once

#include <powpp/types.h>
#include <powpp/network.h>
#include <powpp/solver.h>
#include <powpp/solution.h>

namespace powpp {
	class SolverFastDecoupled : public Solver {
	public:
		explicit SolverFastDecoupled(const Network& network);

		bool canSolve() const override;

		const SolutionPolar& getSolution() const { return sol; }

	protected:
		Result doSolve() override;

	private:
		void jacobian(Mat& J1, Mat& J4, const SpMatCx& Y);

		const Network& network;

		SolutionPolar sol;
	};
}

#pragma once

#include <powpp/types.h>
#include <powpp/network.h>
#include <powpp/solver.h>
#include <powpp/solution.h>

namespace powpp {
	class SolverNewtonRect : public Solver {
	public:
		explicit SolverNewtonRect(const Network& network);

		bool canSolve() const override;

		const SolutionRect& getSolution() const { return sol; }

	protected:
		Result doSolve() override;

	private:
		void jacobian(Mat& J, const SpMatCx& Y, const VecCx& V);

		const Network& network;

		SolutionRect sol;
	};
}

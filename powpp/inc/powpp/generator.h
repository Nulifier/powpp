#pragma once

#include <powpp/types.h>

namespace powpp {
	/**
	 * A generator supplying real power into the system.
	 */
	struct Generator {
		Generator(BusId bus, Scalar realPower, Scalar voltageSetPoint, Scalar minReactive,
		          Scalar maxReactive)
		    : bus(bus), realPower(realPower), voltageSetPoint(voltageSetPoint), minReactive(minReactive),
		      maxReactive(maxReactive) {}

		/// Bus this generator is connected to.
		BusId bus;

		/// Real power this generator can provide.
		Scalar realPower;

		/// Voltage magnitude setpoint
		Scalar voltageSetPoint;

		/// The minimum reactive power that can be supplied by this generator.
		Scalar minReactive;

		/// The maximum reactive power that can be supplied by this generator.
		Scalar maxReactive;
	};
}

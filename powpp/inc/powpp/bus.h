#pragma once

#include "powpp/types.h"

namespace powpp {
	/**
	 * A busbar to which Loads, Lines or Generators can be connected.
	 */
	struct Bus {
		explicit Bus(Scalar nominalVoltage) : nominalVoltage(nominalVoltage) {}

		/// The nominal voltage of this busbar in volts.
		Scalar nominalVoltage;
	};
}

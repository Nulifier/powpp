#pragma once

#include <limits>
#include <powpp/types.h>

/**
 * A series of electrical math helper functions.
 * @note These all work in per unit as well except where specified.
 */
namespace powpp::math {
	/// The smallest positive normal value.
	constexpr Scalar MIN_NORMAL = 0x1.0p-1022;

	/// Checks if two numbers are close to each other.
	constexpr bool nearlyEqual(Scalar a, Scalar b, Scalar epsilon = 0.00001) {
		const Scalar absA = std::fabs(a);
		const Scalar absB = std::fabs(b);
		const Scalar diff = std::fabs(a - b);

		if (a == b) {	// Shortcut, handle infinity
			return true;
		}
		else if (a == 0 || b == 0 || (absA + absB < MIN_NORMAL)) {
			// a or b are zero or very close to it
			// relative error is less meaningful here
			return diff < (epsilon * MIN_NORMAL);
		}
		else {
			return diff / std::min(absA + absB, std::numeric_limits<Scalar>::max()) < epsilon;
		}
	}

	/**
	 * Converts from frequency in Hz to angular frequency.
	 */
	constexpr Scalar angularFrequency(Scalar frequency) {
		return 2 * Pi * frequency;
	}

	/**
	 * Calculates the impedance of a resistor.
	 * @param resistance The resistance in ohms.
	 */
	constexpr Complex resistorImpedance(Scalar resistance) {
		return {resistance, 0.0};
	}

	/**
	 * Caclulates the impedance of a capacitor.
	 * @param capacitance The capacitance in farads (F).
	 * @param frequency The frequency in Hz.
	 */
	constexpr Complex capacitorImpedance(Scalar capacitance, Scalar frequency) {
		return {0.0, -1.0 / capacitance / angularFrequency(frequency)};
	}

	/**
	 * Caclulates the impedance of an inductor.
	 * @param inductance The inductance in henries (H).
	 * @param frequency The frequency in Hz.
	 */
	constexpr Complex inductorImpedance(Scalar inductance, Scalar frequency) {
		return {0.0, inductance * angularFrequency(frequency)};
	}

	/**
	 * Converts admittance (Y) to impedance (Z)
	 */
	constexpr Complex admittanceToImpedance(Complex admittance) {
		return Complex(1.0, 0.0) / admittance;
	}

	/**
	 * Convert impedance (Z) to admittance (Y).
	 */
	constexpr Complex impedanceToAdmittance(Complex inductance) {
		return Complex(1.0, 0.0) / inductance;
	}
}

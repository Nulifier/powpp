#pragma once

#include <complex>
#include <powpp/types.h>
#include <utility>

namespace powpp {
	struct SolutionRect {
		SolutionRect(VecCx V, VecCx S) : V(std::move(V)), S(std::move(S)) {}

		VecCx V;
		VecCx S;

		Complex getV(Index k) const { return V[k]; }
		Scalar getVreal(Index k) const { return V[k].real(); }
		Scalar getVimag(Index k) const { return V[k].imag(); }
		Scalar getVmag(Index k) const { return std::abs(V[k]); }
		Scalar getVarg(Index k) const { return std::arg(V[k]); }
		Complex getS(Index k) const { return S[k]; }
		Scalar getP(Index k) const { return S[k].real(); }
		Scalar getQ(Index k) const { return S[k].imag(); }
	};

	struct SolutionPolar {
		SolutionPolar(Vec Vmag, Vec Varg, VecCx S) : Vmag(std::move(Vmag)), Varg(std::move(Varg)), S(std::move(S)) {}

		Vec Vmag;
		Vec Varg;
		VecCx S;

		Complex getV(Index k) const { return std::polar(Vmag[k], Varg[k]); }
		Scalar getVreal(Index k) const { return Vmag[k] * std::cos(Varg[k]); }
		Scalar getVimag(Index k) const { return Vmag[k] * std::sin(Varg[k]); }
		Scalar getVmag(Index k) const { return Vmag[k]; }
		Scalar getVarg(Index k) const { return Varg[k]; }
		Complex getS(Index k) const { return S[k]; }
		Scalar getP(Index k) const { return S[k].real(); }
		Scalar getQ(Index k) const { return S[k].imag(); }
	};
}

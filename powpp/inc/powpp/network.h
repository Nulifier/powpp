#pragma once

#include <map>
#include <unordered_map>
#include <powpp/types.h>
#include <powpp/bus.h>
#include <powpp/generator.h>
#include <powpp/line.h>
#include <powpp/load.h>
#include <powpp/shunt.h>
#include <powpp/transformer.h>

namespace powpp {
	/**
	 * A system of Busbars, Loads and Generators used to model and electrical system.
	 */
	class Network {
	public:
		using Buses = std::map<BusId, Bus>;	// We use map so the buses stay in order
		using Lines = std::unordered_map<LineId, Line>;
		using Transformers = std::unordered_map<TransformerId, Transformer>;
		using Loads = std::unordered_map<LoadId, Load>;
		using Generators = std::unordered_map<GeneratorId, Generator>;
		using Shunts = std::unordered_map<ShuntId, Shunt>;

		enum class BusType {
			PD, //< Slack bus
			PV, //< Generator bus
			PQ  //< Load bus
		};

		Buses buses;
		Lines lines;
		Transformers transformers;
		Loads loads;
		Generators generators;
		Shunts shunts;

		/// Select a reasonable bus to be the slack bus.
		BusId selectSlackBus() const;

		/// Determine the maximum real power.
		Scalar getMaxRealPower() const;

		/// Checks if the network and components make sense.
		bool isValid() const;

		/// This should be called any time the network is modified.
		void compile() { compile(selectSlackBus(), getMaxRealPower()); }
		/// @copydoc Network::compile()
		void compile(BusId slackBus, Scalar Sbase);

		Scalar getSbase() const { return Sbase; }

		/// Converts from BusId to Index.
		Index getBusIndex(BusId id) const { return indexLookup.at(id); }

		/// Get the admittance matrix.
		const SpMatCx& getY() const { return Y; }

		const MatCx& getZ() const {
			if (Z.size() == 0) {
				updateZ();
			}
			return Z;
		}

		const MatCx& getZred() const {
			if (Zred.size() == 0) {
				updateZred();
			}
			return Zred;
		}

		BusType getBusType(BusId id) const;

		const Indices& PD() const { return pdIndices; }
		const Indices& PV() const { return pvIndices; }
		const Indices& PQ() const { return pqIndices; }
		const Indices& PVPQ() const { return pvpqIndices; }
		Index n() const { return static_cast<Index>(buses.size()); }
		Index nPD() const { return static_cast<Index>(pdIndices.size()); }
		Index nPV() const { return static_cast<Index>(pvIndices.size()); }
		Index nPQ() const { return static_cast<Index>(pqIndices.size()); }
		Index nPVPQ() const { return static_cast<Index>(pvpqIndices.size()); }

		const VecCx& getConnectedPower() const { return connectedPower; }
		const Vec& getInitialVoltageMagnitudes() const { return initalVoltageMagnitudes; }

	private:
		/// Checks if a BusId exists.
		bool isBusIdValid(BusId id) const { return buses.count(id) > 0; }

		void updateZ() const;

		void updateZred() const;

		/**
		 * Lookup from BusId to Index.
		 * @note Updated in compile().
		 */
		std::unordered_map<BusId, Index> indexLookup;

		Scalar Sbase;
		SpMatCx Y;
		mutable MatCx Z;
		mutable MatCx Zred;

		std::unordered_map<BusId, BusType> busTypes;

		Indices pdIndices;
		Indices pvIndices;
		Indices pqIndices;
		Indices pvpqIndices;

		VecCx connectedPower;
		Vec initalVoltageMagnitudes;
	};
}

#pragma once

#include <powpp/types.h>
#include <powpp/network.h>
#include <powpp/solver.h>
#include <powpp/solution.h>

namespace powpp {
	class SolverNewtonPolar : public Solver {
	public:
		explicit SolverNewtonPolar(const Network& network);

		bool canSolve() const override;

		const SolutionPolar& getSolution() const { return sol; }

	protected:
		Result doSolve() override;

	private:
		void jacobian(Mat& J, const SpMatCx& Y, const Vec& Vmag, const Vec& Varg);

		const Network& network;

		SolutionPolar sol;
	};
}

#pragma once

#include "powpp/types.h"

namespace powpp {
	struct Shunt {
		Shunt(BusId bus, Complex impedance) : bus(bus), impedance(impedance) {}

		BusId bus;
		Complex impedance;
	};
}

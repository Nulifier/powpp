#pragma once

#define STRONG_TYPEDEF(Name, T)                                                \
	struct Name {                                                              \
		using Type = T;                                                        \
		Name() : value() {}                                                    \
		explicit Name(const T v) : value(v) {}                                 \
		explicit operator T&() { return value; }                               \
		explicit operator const T&() const { return value; }                   \
		bool operator==(const Name& rhs) const { return value == rhs.value; }  \
		bool operator!=(const Name& rhs) const { return value != rhs.value; }  \
		bool operator>(const Name& rhs) const { return value > rhs.value; }    \
		bool operator<(const Name& rhs) const { return value < rhs.value; }    \
                                                                               \
	private:                                                                   \
		T value;                                                               \
	}

#define STRONG_TYPEDEF_HASH(Name)                                              \
	template <> struct std::hash<Name> {                                       \
		std::size_t operator()(const Name& v) const noexcept {                 \
			return std::hash<Name::Type>{}(static_cast<Name::Type>(v));        \
		};                                                                     \
	}

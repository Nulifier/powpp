#pragma once

#include <limits>
#include <ostream>
#include <string>
#include <vector>
#include <powpp/types.h>

namespace powpp {
	struct TableFormat {
		bool complexJ = true;
	};

	class Table {
	public:
		explicit Table(const TableFormat& format = TableFormat())
		    : format(format) {}

		template <typename Derived>
		void setData(const Eigen::EigenBase<Derived>& obj);

		std::string str() const;

		void print() const;

		friend std::ostream& operator<<(std::ostream& os, const Table& t);

	private:
		struct Col {
			/// Maximum width of a field in this column.
			int maxWidth = 0;
		};

		struct Row {
			explicit Row(size_t width = 0) : data(width) {}
			std::vector<std::string> data;
		};

		std::string toString(Scalar value) const;
		std::string toString(Complex value) const;

		/**
		 * The maximum length that it takes to represent a scalar.
		 * @note This is the value used in the GNU std::to_string function
		 */
		static constexpr size_t SCALAR_LEN = std::numeric_limits<Scalar>::max_exponent10 + 20;

		TableFormat format;
		std::vector<Col> cols;
		std::vector<Row> rows;
	};

	std::ostream& operator<<(std::ostream& os, const powpp::Table& t);
}

template <typename Derived>
void powpp::Table::setData(const Eigen::EigenBase<Derived>& obj) {
	using Eigen::Index;

	cols.resize(obj.cols());
	rows.resize(obj.rows());
	for (Index rowIdx = 0; rowIdx < obj.rows(); ++rowIdx) {
		Row& row = rows[rowIdx];
		row.data.resize(obj.cols());

		for (Index colIdx = 0; colIdx < obj.cols(); ++colIdx) {
			std::string str = toString(obj.derived().coeff(rowIdx, colIdx));

			if (str.size() > cols[colIdx].maxWidth) {
				cols[colIdx].maxWidth = static_cast<int>(str.size());
			}

			row.data[colIdx] = std::move(str);
		}
	}
}

#pragma once

#include "powpp/types.h"

namespace powpp {
	/**
	 * A load that absorbs real power from the network.
	 */
	struct Load {
		Load(BusId bus, Complex power) : bus(bus), power(power) {}

		/// Bus this load is connected to.
		BusId bus;

		/// The power consumed by this load.
		Complex power;
	};
}

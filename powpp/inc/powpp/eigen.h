#pragma once

#include <powpp/types.h>

/// Utilities for Eigen
namespace powpp::eigen {
	/**
	 * A sequence for use with the matrix slice operator that skips one entry.
	 */
	struct seqSkip {
		seqSkip(Index toSkip, Index newSize)
		    : toSkip(toSkip), newSize(newSize) {}

		Index toSkip;
		Index newSize;

		Index size() const { return newSize; }
		Index operator[](Index i) const { return i < toSkip ? i : i + 1; }
	};

	/**
	 * Removes a row and a column from a sparse matrix.
	 */
	template <typename A, typename B>
	void removeCross(Eigen::SparseMatrixBase<A>& out,
	                 const Eigen::SparseMatrixBase<B>& in, Index row,
	                 Index col) {

		const Index rows = in.rows();
		const Index cols = in.cols();

		assert(row >= 0 && row < rows && col >= 0 && col < cols);

		out.derived().resize(rows - 1, cols - 1);

		for (Index k = 0; k < in.outerSize(); ++k) {
			for (typename B::InnerIterator it(in.derived(), k); it; ++it) {
				const Index srcRow = it.row();
				const Index srcCol = it.col();

				// Skip the cross
				if (srcRow == row || srcCol == col) {
					continue;
				}

				const Index newRow = srcRow < row ? srcRow : srcRow - 1;
				const Index newCol = srcCol < col ? srcCol : srcCol - 1;

				out.derived().coeffRef(newRow, newCol) = it.value();
			}
		}
	}

	/**
	 * Removes a row and a column from a matrix in place.
	 */
	template <typename Derived>
	void removeCross(Eigen::MatrixBase<Derived>& mat, Index row, Index col) {
	    const Index rows = mat.rows();
	    const Index cols = mat.cols();
		const Index rowsLeft = rows - row - 1;
		const Index colsLeft = cols - col - 1;

	    assert(row >= 0 && row < rows && col >= 0 && col < cols);

		// We divide the matrix in to 4 blocks
		// B1 B2
		// B3 B4
		// B1 doesn't need to be touched

		// Move B2 if we aren't removing the last column
		if (col != cols - 1) {
			mat.block(0, col, row, colsLeft) = mat.topRightCorner(row, colsLeft);
		}

		// Move B3
		if (row != rows - 1) {
			mat.block(row, 0, rowsLeft, col) = mat.bottomLeftCorner(rowsLeft, col);
		}

		// Move B4
		if ((col != cols - 1) && (row != rows - 1)) {
			mat.block(row, col, rowsLeft, colsLeft) = mat.bottomRightCorner(rowsLeft, colsLeft);
		}

		// Resize, leaving existing coefficients
		mat.derived().conservativeResize(rows - 1, cols - 1);
	}

	/**
	 * Adds an empty row and column to a matrix in place.
	 */
	template <typename Derived>
	void expandCross(Eigen::MatrixBase<Derived>& mat, Index row, Index col) {
	    const Index rows = mat.rows();
	    const Index cols = mat.cols();
		const Index rowsLeft = rows - row;
		const Index colsLeft = cols - col;

	    assert(row >= 0 && row <= rows && col >= 0 && col <= cols);

		// We divide the matrix in to 4 blocks
		// B1 B2
		// B3 B4
		// B1 doesn't need to be touched

		// Resize, leaving existing coefficients
		mat.derived().conservativeResize(rows + 1, cols + 1);

		// Move B4
		mat.block(row + 1, col + 1, rowsLeft, colsLeft) = mat.block(row, col, rowsLeft, colsLeft).eval();

		// Move B2
		mat.block(0, col + 1, row, colsLeft) = mat.block(0, col, row, colsLeft).eval();

		// Move B3
		mat.block(row + 1, 0, rowsLeft, col) = mat.block(row, 0, rowsLeft, col).eval();

		mat.row(row) = Mat::Zero(1, cols + 1);
		mat.col(col) = Mat::Zero(rows + 1, 1);
	}
}

#pragma once

#include <powpp/types.h>
#include <powpp/network.h>
#include <powpp/solver.h>
#include <powpp/solution.h>

namespace powpp {
	class SolverGaussSeidel : public Solver {
	public:
		explicit SolverGaussSeidel(const Network& network, Scalar accelFactor = 1.6);

		bool canSolve() const override;

		const SolutionRect& getSolution() const { return sol; }

	protected:
		Result doSolve() override;

	private:
		const Network& network;
		const Scalar accelFactor;

		SolutionRect sol;
	};
}

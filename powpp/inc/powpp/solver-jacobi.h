#pragma once

#include <powpp/types.h>
#include <powpp/network.h>
#include <powpp/solver.h>
#include <powpp/solution.h>

namespace powpp {
	class SolverJacobi : public Solver {
	public:
		explicit SolverJacobi(const Network& network);

		bool canSolve() const override;

		const SolutionRect& getSolution() const { return sol; }

	protected:
		Result doSolve() override;

	private:
		const Network& network;

		SolutionRect sol;
	};
}

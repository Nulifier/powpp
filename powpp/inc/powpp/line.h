#pragma once

#include <powpp/types.h>

namespace powpp {
	struct LineType {
		/**
		 * Constructs a LineType.
		 * @param r The resistance per distance unit.
		 * @param x The reactance per distance unit.
		 * @param b The susceptance per distance unit.
		 */
		LineType(Scalar r, Scalar x, Scalar b = 1e-9)
		    : impedance(r, x), shuntAdmittance(0.0, b) {}

		/// Impedance per distance unit.
		Complex impedance;

		/// Shunt admittance per distance unit.
		Complex shuntAdmittance;
	};

	/**
	 * A connection between two Busses.
	 */
	struct Line {
		/// Constructs a line using a pre-determined line type.
		Line(BusId busA, BusId busB, const LineType& type, Scalar length);
		/// Constructs a line using specific values.
		Line(BusId busA, BusId busB, Scalar r, Scalar x, Scalar b, Scalar length = 1.0);

		BusId busA;
		BusId busB;

		void setLineType(const LineType& type);
		void setLength(Scalar newLength);

		Scalar getLength() const { return length; }
		const Complex& getImpedance() const { return impedance; }
		const Complex& getShuntAdmittance() const { return shuntAdmittance; }

	private:
		/**
		 * Length of the line in km.
		 * @note This cannot be changed as it will invalidate the impedance and
		 * admittances.
		 */
		Scalar length;

		/**
		 * Impedance of the line.
		 * @note Calculated base on LineType and length.
		 */
		Complex impedance;

		/**
		 * Shunt admittance of the line.
		 * @note Calculated base on LineType and length.
		 */
		Complex shuntAdmittance;
	};
}

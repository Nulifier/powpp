#pragma once

#include "powpp/types.h"

namespace powpp {
	/**
	 * Represents a type of transformer.
	 * @note All values are in per unit.
	 */
	struct TransformerType {
		TransformerType(Complex leakageImpedance, Complex magnetizingImpedance)
		    : leakageImpedance(leakageImpedance),
		      magnetizingImpedance(magnetizingImpedance) {}

		/// Leakage impedance in per unit.
		Complex leakageImpedance;

		/// Magnetizing impedance in per unit.
		Complex magnetizingImpedance;

		/// Phase shift, normally 30°
		Scalar phaseShift = Pi / 6.0;
	};

	struct Transformer {
		Transformer(BusId busHV, BusId busLV, const TransformerType& type);

		BusId busHV;
		BusId busLV;

		/// Tap position, value should be around 1.
		Scalar tapPosition = 1.0;

		/// Phase shift, normally 30°
		Scalar phaseShift;

		/// Leakage impedance in per unit.
		Complex leakageImpedance;

		/// Magnetizing impedance in per unit.
		Complex magnetizingImpedance;
	};
}

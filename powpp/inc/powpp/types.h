#pragma once

#include <vector>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <powpp/strong-typedef.h>

namespace powpp {
	STRONG_TYPEDEF(BusId, unsigned int);
	STRONG_TYPEDEF(LineId, unsigned int);
	STRONG_TYPEDEF(TransformerId, unsigned int);
	STRONG_TYPEDEF(LoadId, unsigned int);
	STRONG_TYPEDEF(GeneratorId, unsigned int);
	STRONG_TYPEDEF(ShuntId, unsigned int);

	using Scalar = double;
	using Complex = std::complex<Scalar>;

	using Index = Eigen::Index;
	using Indices = std::vector<Index>;
	using Vec = Eigen::VectorXd;
	using VecCx = Eigen::VectorXcd;
	using Mat = Eigen::MatrixXd;
	using MatCx = Eigen::MatrixXcd;
	using SpMat = Eigen::SparseMatrix<Scalar>;
	using SpMatCx = Eigen::SparseMatrix<Complex>;

	static constexpr Scalar Pi = 3.1415926535897932384626433832795;
}

STRONG_TYPEDEF_HASH(powpp::BusId);
STRONG_TYPEDEF_HASH(powpp::LineId);
STRONG_TYPEDEF_HASH(powpp::TransformerId);
STRONG_TYPEDEF_HASH(powpp::LoadId);
STRONG_TYPEDEF_HASH(powpp::GeneratorId);
STRONG_TYPEDEF_HASH(powpp::ShuntId);

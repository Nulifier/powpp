set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
set(DOXYGEN_STRIP_FROM_PATH
	${CMAKE_SOURCE_DIR}/powpp/inc
	${CMAKE_SOURCE_DIR}/powpp/src
)
set(DOXYGEN_GENERATE_TREEVIEW YES)
#set(DOXYGEN_HTML_STYLESHEET "doxygen-awesome.css")

doxygen_add_docs(docs
	${CMAKE_SOURCE_DIR}/powpp/inc
	${CMAKE_SOURCE_DIR}/powpp/src
)
